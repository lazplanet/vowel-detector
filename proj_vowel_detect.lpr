program proj_vowel_detect;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes
  { you can add units after this };

var
  letter: char;
  foundvowel: boolean = false;

begin

  WriteLn('Vowel detection program!');
  WriteLn('You will be asked until you enter a vowel!');
  WriteLn('');
  WriteLn('Please enter a letter:');
  ReadLn(letter);
  letter:=lowercase(letter);

  while foundvowel = false do
  begin
    case letter of
       'a', 'e', 'i', 'o', 'u': foundvowel := True;
       else foundvowel := False;
    end;

    if foundvowel = True then begin
      WriteLn('Thank you. It is a vowel. It can pronounce itself.');
      WriteLn('Press ENTER to end.');
    end else begin
      WriteLn('');
      WriteLn('Wrong!! Please Enter a vowel:');
      ReadLn(letter);
      letter:=lowercase(letter);
    end;

  end;

  ReadLn(); // we keep the console window from closing

end.
